# Ansible stuff

- create vms with ubuntu-20.04.3-live-server-amd64.iso
- start vms
- get vm ips: "nmap iprange", e.g "nmap 192.168.122.0/24" (or `ip a` in vms)
- connect to vm: `ssh user@ip`, e.g. `ssh test-mk@192.168.122.157`
- create ssh key for ansible, e.g. `ssh-keygen -t ed25519`
- send ssh key to server, e.g. `ssh-copy-id -i ~/.ssh/id_ed25519_ansible-01.pub test-mk@192.168.122.157`
- connect to vm: `ssh test-mk@192.168.122.157 -i ~/.ssh/id_ed25519_ansible-01`
- check status of all vms: `ansible all -m ping
- elevate privileges: `--become`
- ask for root password: `--ask-become-pass`
- use a module: `-m name`, e.g. `-m apt`
- use an argument: `-a arg`, e.g. `-a name=vim`
- update all vms: `ansible all -m apt -a update_cache=true --become --ask-become-pass`
- upgrade all vms: `ansible all -m apt -a upgrade=yes --become --ask-become-pass`
- add conditional: `when`
- use `package` instead of specific package manager like `apt`
- create encrypted file: `ansible-vault encrypt inventory`
- edit encrypted file: `ansible-vault edit inventory`
- run with encrypted file: `ansible-playbook run.yml --ask-vault-pass

## Pihole

- changes:
  - standard blocklist: https://raw.githubusercontent.com/mhhakim/pihole-blocklist/master/list.txt
  - upstream dns: Quad9 (unfiltered, no DNSSEC), 5.9.164.112, 91.239.100.100
  - local dns: set all local domains to their ip

## Hints

- create one setup playbook, e.g. `bootstrap.yml`, with one first update and user setup (create user, authorized_key and sudoers file)
- be cautious which part of the ssh pair to use (private or public)

## Further readings

- install on digital ocean: https://docs.ansible.com/ansible/latest/collections/community/digitalocean/digital_ocean_droplet_module.html#ansible-collections-community-digitalocean-digital-ocean-droplet-module
